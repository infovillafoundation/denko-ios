//
//  LanguageSelectionViewController.m
//  DenkoStation
//
//  Created by Sandah Aung on 3/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "LanguageSelectionViewController.h"

@interface LanguageSelectionViewController ()

@end

@implementation LanguageSelectionViewController

@synthesize countDownFlag;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    height = [UIScreen mainScreen].bounds.size.height;
    width = [UIScreen mainScreen].bounds.size.width;
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self writeChooseLanguageText:@"Choose Language" x:0 y:40];
    [self writeCurrentLanguageText];
    [self placeLanguageFlagAtY:96 screenWidth:width];
    
    [self placeLanguageButtonForMyanmarAtY:219 screenWidth:width];
    [self placeLanguageButtonForEnglishAtY:287 screenWidth:width];
    
    hasRun = NO;
    theLock = [[NSLock alloc] init];
    i = 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)writeChooseLanguageText:(NSString *) textToWrite x: (int) x y: (int) y {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(x, y+offset, width, 36)];
    text.text = textToWrite;
    text.textColor = [UIColor colorWithRed:0.48627450980392 green:0.72941176470588 blue:0.25882352941176 alpha:1.0];
    text.textAlignment = NSTextAlignmentCenter;
    [text setFont:[UIFont fontWithName:@"OpenSans" size:25]];
    [self.view addSubview:text];
}

- (void)placeLanguageFlagAtY: (int) y screenWidth: (int) screenWidth {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    
    NSString *languageImageName = nil;
    if ([self isMyanmar]) {
        languageImageName = @"MyanmarFlagBig";
    }
    else {
        languageImageName = @"UnitedKingdomFlagBig";
    }
    
    UIImage *languageImage = [UIImage imageNamed:languageImageName];
    languageFlag = [[UIImageView alloc] initWithImage:languageImage];
    languageFlag.frame = CGRectMake(0, y+2*offset, languageFlag.frame.size.width, languageFlag.frame.size.height);
    languageFlag.center = CGPointMake(screenWidth / 2, languageFlag.center.y);
    [self.view addSubview:languageFlag];
}

- (void)changeLanguageFlag:(NSString *) languageImageName {
    UIImage *languageImage = [UIImage imageNamed:languageImageName];
    int imageWidth = languageImage.size.width;
    int imageHeight = languageImage.size.height;
    [languageFlag setImage:languageImage];
    languageFlag.frame = CGRectMake(languageFlag.frame.origin.x, languageFlag.frame.origin.y, imageWidth, imageHeight);
    languageFlag.center = CGPointMake(width / 2, languageFlag.center.y);
    
}

- (void)writeCurrentLanguageText {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    
    int y = 0;
    if (width == 320) {
        if ([self isMyanmar]) {
            y = 172;
        } else {
            y = 163;
        }
    } else {
        if ([self isMyanmar]) {
            y = 162;
        } else {
            y = 153;
        }
    }
    
    languageText = [[UILabel alloc] initWithFrame:CGRectMake(0, y+3*offset, width, 36)];
    languageText.frame = CGRectMake(0, y+3*offset, width, 36);
    languageText.textColor = [UIColor blackColor];
    languageText.textAlignment = NSTextAlignmentCenter;
    if ([self isMyanmar]) {
        [languageText setFont:[UIFont fontWithName:@"WinInnwa" size:25]];
        languageText.text = @"jrefrm";
    } else {
        [languageText setFont:[UIFont fontWithName:@"OpenSans" size:20]];
        languageText.text = @"English";
    }
    
    [self.view addSubview:languageText];
}

- (void)updateCurrentLanguageText {
    if ([self isMyanmar]) {
        [languageText setFont:[UIFont fontWithName:@"WinInnwa" size:25]];
        languageText.text = @"jrefrm";
    } else {
        [languageText setFont:[UIFont fontWithName:@"OpenSans" size:20]];
        languageText.text = @"English";
    }
}

- (void)placeLanguageButtonForMyanmarAtY: (int) y screenWidth: (int) screenWidth {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    UIImage *buttonImage = [UIImage imageNamed:@"MyanmarLanguageButton"];
    UIImage *buttonImagePressed = [UIImage imageNamed:@"MyanmarLanguageButtonPressed"];
    UIButton *languageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [languageButton setImage:buttonImage forState:UIControlStateNormal];
    [languageButton setImage:buttonImagePressed forState:UIControlStateHighlighted];
    [languageButton sizeToFit];
    languageButton.frame = CGRectMake(0, y+4*offset, languageButton.frame.size.width, languageButton.frame.size.height);
    languageButton.center = CGPointMake(screenWidth / 2, languageButton.center.y);
    [languageButton addTarget:self action:@selector(changeLanguageToMyanmar) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *changeLanguageText = [[UILabel alloc] initWithFrame:languageButton.frame];
    changeLanguageText.text = @"jrefrmpmajymif;&ef ESdyfyg";
    changeLanguageText.textColor = [UIColor whiteColor];
    changeLanguageText.textAlignment = NSTextAlignmentCenter;
    [changeLanguageText setFont:[UIFont fontWithName:@"WinInnwa" size:29]];
    
    [self.view addSubview:languageButton];
    [self.view addSubview:changeLanguageText];
}

- (void)placeLanguageButtonForEnglishAtY: (int) y screenWidth: (int) screenWidth {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    UIImage *buttonImage = [UIImage imageNamed:@"MyanmarLanguageButton"];
    UIImage *buttonImagePressed = [UIImage imageNamed:@"MyanmarLanguageButtonPressed"];
    UIButton *languageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [languageButton setImage:buttonImage forState:UIControlStateNormal];
    [languageButton setImage:buttonImagePressed forState:UIControlStateHighlighted];
    [languageButton sizeToFit];
    languageButton.frame = CGRectMake(0, y+5*offset, languageButton.frame.size.width, languageButton.frame.size.height);
    languageButton.center = CGPointMake(screenWidth / 2, languageButton.center.y);
    [languageButton addTarget:self action:@selector(changeLanguageToEnglish) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *changeLanguageText = [[UILabel alloc] initWithFrame:languageButton.frame];
    changeLanguageText.text = @"Set Language to English";
    changeLanguageText.textColor = [UIColor whiteColor];
    changeLanguageText.textAlignment = NSTextAlignmentCenter;
    [changeLanguageText setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    
    [self.view addSubview:languageButton];
    [self.view addSubview:changeLanguageText];
}

- (BOOL)isMyanmar {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"myanmar"];
    return state;
}

- (void)changeLanguageToEnglish {
    [theLock lock];
    if (hasRun == YES) {
        [theLock unlock];
        return;
    }
    hasRun = YES;
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:false forKey:@"myanmar"];
    [userdefaults synchronize];
    
    [self updateCurrentLanguageText];
    if ([self isMyanmar]) {
        [self changeLanguageFlag:@"MyanmarFlagBig"];
    } else {
        [self changeLanguageFlag:@"UnitedKingdomFlagBig"];
    }
    
    [self startCountDown];
    
    [theLock unlock];

}

- (void)changeLanguageToMyanmar {
    [theLock lock];
    if (hasRun == YES) {
        [theLock unlock];
        return;
    }
    hasRun = YES;
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:true forKey:@"myanmar"];
    [userdefaults synchronize];
    
    [self updateCurrentLanguageText];
    if ([self isMyanmar]) {
        [self changeLanguageFlag:@"MyanmarFlagBig"];
    } else {
        [self changeLanguageFlag:@"UnitedKingdomFlagBig"];
    }
    
    [self startCountDown];
    
    [theLock unlock];

}

- (void)startCountDown {
    [self countDown];
}

- (void)countDown {
    int offset = 0;
    if (height == 568) {
        offset += 10;
    } else if (height == 1024) {
        offset += 40;
    }
    if (i == 3) {
        i--;
        UIImage *three = [UIImage imageNamed:@"Three"];
        countDownFlag = [[UIImageView alloc] initWithImage:three];
        countDownFlag.frame = CGRectMake(0, 370+6*offset, countDownFlag.frame.size.width, countDownFlag.frame.size.height);
        countDownFlag.center = CGPointMake(width / 2, countDownFlag.center.y);
        [self.view addSubview:countDownFlag];
        [self performSelector:@selector(countDown) withObject:nil afterDelay:0.5];
    } else if (i == 2) {
        i--;
        UIImage *two = [UIImage imageNamed:@"Two"];
        [countDownFlag setImage:two];
        [self performSelector:@selector(countDown) withObject:nil afterDelay:0.5];
    } else if (i == 1) {
        i--;
        UIImage *one = [UIImage imageNamed:@"One"];
        [countDownFlag setImage:one];
        [self performSelector:@selector(countDown) withObject:nil afterDelay:0.5];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
