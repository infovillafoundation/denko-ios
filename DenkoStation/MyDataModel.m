//
//  MyDataModel.m
//  DenkoStation
//
//  Created by aztunwin on 10/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyDataModel.h"

@implementation MyDataModel

static MyDataModel *sharedDataModel = nil;

+ (MyDataModel *) sharedDataModel
{
    
    @synchronized(self)
    {
        if (sharedDataModel == nil)
        {
            sharedDataModel = [[MyDataModel alloc] init];
        }
    }
    return sharedDataModel;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

@end
