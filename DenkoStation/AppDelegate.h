//
//  AppDelegate.h
//  DenkoStation
//
//  Created by Sandah Aung on 20/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#include "JVFloatingDrawerViewController.h"
#include "JVFloatingDrawerSpringAnimator.h"
#include "DrawerViewController.h"
#include "MainViewController.h"
#import "PersistentStack.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UINavigationController *navigationController;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) JVFloatingDrawerViewController *drawerViewController;
@property (nonatomic, strong) JVFloatingDrawerSpringAnimator *drawerAnimator;

@property (nonatomic, strong) DrawerViewController *leftDrawerViewController;
@property (nonatomic, strong) MainViewController *mainViewController;

@property (nonatomic, strong) PersistentStack* persistentStack;

@property (nonatomic) BOOL splashScreenLoaded;


+ (AppDelegate *)globalDelegate;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)toggleLeftDrawer:(id)sender animated:(BOOL)animated;

- (void)replaceCentreViewWithViewAt:(int) index;


@end

