//
//  DataPrice.h
//  DenkoStation
//
//  Created by Sandah Aung on 21/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Price.h"

@interface DataPrice : NSManagedObject

@property (nonatomic, retain) NSNumber *id;
@property (nonatomic, retain) NSNumber *ron95;
@property (nonatomic, retain) NSNumber *ron92;
@property (nonatomic, retain) NSNumber *dieselNormal;
@property (nonatomic, retain) NSNumber *dieselSpecial;
@property (nonatomic, retain) NSDate *postDate;

+ (instancetype)insertDataPriceWithPrice:(Price*)price
                  inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (instancetype)fetchDataPriceInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (Price*)fetchPriceInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSString*)entityName;

@end
