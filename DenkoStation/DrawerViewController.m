//
//  DrawerViewController.m
//  DenkoStation
//
//  Created by Sandah Aung on 23/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "DrawerViewController.h"
#import "AppDelegate.h"

#define ICON_TAG 1
#define LABEL_TAG 2

@interface DrawerViewController ()

@end

@implementation DrawerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.95686274509804 green:0.46274509803922 blue:0.19607843137255 alpha:1]];
    
    int height = [UIScreen mainScreen].bounds.size.height;
    
    UIView *rectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 75, height)];
    rectView.backgroundColor = [UIColor colorWithRed:0.94901960784314 green:0.4078431372549 blue:0.13333333333333 alpha:1];
    [self.view addSubview:rectView];
    
    UIImageView *monoDenko = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MonoDenko"]];
    monoDenko.frame = CGRectMake(12, height -172, 50, 159);
    [self.view addSubview:monoDenko];
    
    if (height == 1024) {
        [self placeNavIcon:@"NavIconWhite" x:7 y:55];
    } else {
        [self placeNavIcon:@"NavIconWhite" x:7 y:30];
    }
    [self placeNavigator];
    
    self.labels = [[NSMutableArray alloc] init];
    
    NSTimer* timer = [NSTimer timerWithTimeInterval:30.0f target:self selector:@selector(reloadTable) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)placeNavIcon:(NSString *) imageName x:(int)x y: (int)y {
    UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image.frame = CGRectMake(x, y, 60, 60);
    [self.view addSubview:image];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openDrawer)];
    singleTap.numberOfTapsRequired = 1;
    [image setUserInteractionEnabled:YES];
    [image addGestureRecognizer:singleTap];
}

- (void)placeNavigator {
    int height = [UIScreen mainScreen].bounds.size.height;
    int y = 100;
    if (height == 1024)
        y = 200;
    else if (height == 568)
        y = 110;
    CGRect navigationTableBounds = CGRectMake(0, y, 280, 180);
    self.navigationTableView = [[UITableView alloc] initWithFrame:navigationTableBounds];
    self.navigationTableView.delegate = self;
    self.navigationTableView.dataSource = self;
    self.navigationTableView.backgroundColor = [UIColor clearColor];
    self.navigationTableView.opaque = NO;
    self.navigationTableView.backgroundView = nil;
    [self.navigationTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:self.navigationTableView];
}

- (void)reloadTable {
    NSString *labelNameEn = nil;
    NSString *labelNameMm = nil;
    for (int i = 0; i < 4; i++) {
        switch (i) {
            case 0:
                labelNameEn = @"Home";
                labelNameMm = @"yifr";
                break;
                
            case 1:
                labelNameEn = @"Denko Stations";
                labelNameMm = @"qdkifrsm;";
                break;
                
            case 2:
                labelNameEn = @"Map";
                labelNameMm = @"ajryHk";
                break;
                
            case 3:
                labelNameEn = @"About Denko";
                labelNameMm = @"od&Sd&ef";
                break;
                
            default:
                break;
        }
        UILabel *navigationLabel = [self.labels objectAtIndex:i];
        if ([self isMyanmar]) {
            [navigationLabel setFont:[UIFont fontWithName:@"WinInnwa" size:19]];
            navigationLabel.text = labelNameMm;
        } else {
            [navigationLabel setFont:[UIFont fontWithName:@"OpenSans" size:15]];
            navigationLabel.text = labelNameEn;
        }
    }
}

- (void)openDrawer{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
}

- (BOOL)isMyanmar {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"myanmar"];
    return state;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    UITableViewCell *navigationCellView = nil;

    NSString *imageName = nil;
    NSString *labelNameEn = nil;
    NSString *labelNameMm = nil;
    switch (indexPath.row) {
        case 0:
            imageName = @"Home";
            labelNameEn = @"Home";
            labelNameMm = @"yifr";
            break;
            
        case 1:
            imageName = @"Pump";
            labelNameEn = @"Denko Stations";
            labelNameMm = @"qdkifrsm;";
            break;
            
        case 2:
            imageName = @"Map";
            labelNameEn = @"Map";
            labelNameMm = @"ajryHk";
            break;
            
        case 3:
            imageName = @"Us";
            labelNameEn = @"About Denko";
            labelNameMm = @"od&Sd&ef";
            break;
            
        default:
            break;
    }
    
    if ([tableView isEqual:self.navigationTableView]) {
        static NSString *TableViewCellIdentifier = @"NavigationCell";
        navigationCellView = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
        
        if (navigationCellView == nil) {
            navigationCellView = [[UITableViewCell alloc]
                                  initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:TableViewCellIdentifier];
            
            UIImageView *navigationIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
            CGRect iconFrame = CGRectMake(0, 0, 75, 42.5);
            CGPoint centre = CGPointMake((iconFrame.origin.x + iconFrame.size.width) / 2, (iconFrame.origin.y + iconFrame.size.height) / 2);
            navigationIcon.center = centre;
            [navigationCellView.contentView addSubview:navigationIcon];

            CGRect labelFrame = CGRectMake(100.0, 0.0, 150.0, 42.5);
            UILabel *navigationLabel = [[UILabel alloc] initWithFrame:labelFrame];
            navigationLabel.tag = LABEL_TAG;
            if ([self isMyanmar]) {
                [navigationLabel setFont:[UIFont fontWithName:@"WinInnwa" size:19]];
                navigationLabel.text = labelNameMm;
            } else {
                [navigationLabel setFont:[UIFont fontWithName:@"OpenSans" size:15]];
                navigationLabel.text = labelNameEn;
            }
            navigationLabel.backgroundColor = [UIColor clearColor];
            [navigationLabel setTextColor:[UIColor whiteColor]];
            [navigationCellView.contentView addSubview:navigationLabel];
            [self.labels insertObject:navigationLabel atIndex:indexPath.row];
            
            UIView *bgColorViewNormal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, navigationCellView.frame.size.width, 45)];
            UIView* separatorNormalAbove = [[UIView alloc] initWithFrame:CGRectMake(0, 0, navigationCellView.frame.size.width, 1)];
            UIView *bgColorViewNormalInside = [[UIView alloc] initWithFrame:CGRectMake(0, 1, navigationCellView.frame.size.width, 43.0)];
            UIView* separatorNormalBelow = [[UIView alloc] initWithFrame:CGRectMake(0, 44, navigationCellView.frame.size.width, 1)];
            separatorNormalAbove.backgroundColor = [UIColor clearColor];
            separatorNormalBelow.backgroundColor = [UIColor clearColor];
            [bgColorViewNormal addSubview:separatorNormalAbove];
            [bgColorViewNormal addSubview:bgColorViewNormalInside];
            [bgColorViewNormal addSubview:separatorNormalBelow];
            bgColorViewNormalInside.backgroundColor = [UIColor colorWithRed:0.9843137254902 green:0.69019607843137 blue:0.25098039215686 alpha:1];
            navigationCellView.backgroundView = bgColorViewNormal;
            navigationCellView.backgroundColor = [UIColor clearColor];
            
            UIView *bgColorViewSelected = [[UIView alloc] initWithFrame:CGRectMake(0, 0, navigationCellView.frame.size.width, 45)];
            UIView* separatorSelectedAbove = [[UIView alloc] initWithFrame:CGRectMake(0, 0, navigationCellView.frame.size.width, 1)];
            UIView *bgColorViewSelectedInside = [[UIView alloc] initWithFrame:CGRectMake(0, 1, navigationCellView.frame.size.width, 43.0)];
            UIView* separatorSelectedBelow = [[UIView alloc] initWithFrame:CGRectMake(0, 44, navigationCellView.frame.size.width, 1)];
            separatorSelectedAbove.backgroundColor = [UIColor clearColor];
            separatorSelectedBelow.backgroundColor = [UIColor clearColor];
            [bgColorViewSelected addSubview:separatorSelectedAbove];
            [bgColorViewSelected addSubview:bgColorViewSelectedInside];
            [bgColorViewSelected addSubview:separatorSelectedBelow];
            bgColorViewSelectedInside.backgroundColor = [UIColor colorWithRed:0.97647058823529 green:0.74117647058824 blue:0.39607843137255 alpha:1];
            [navigationCellView setSelectedBackgroundView:bgColorViewSelected];
        }
    }
    
    return navigationCellView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
    [[AppDelegate globalDelegate] replaceCentreViewWithViewAt: indexPath.row];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
