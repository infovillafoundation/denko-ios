//
//  MyDataModel.h
//  DenkoStation
//
//  Created by Sandah Aung on 10/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

@interface MyDataModel : NSObject {
    
}

+ (MyDataModel *) sharedDataModel;

@end