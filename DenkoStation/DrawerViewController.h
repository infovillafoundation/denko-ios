//
//  DrawerViewController.h
//  DenkoStation
//
//  Created by Sandah Aung on 23/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawerViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
}

@property (nonatomic, strong) UITableView *navigationTableView;
@property (nonatomic, strong) NSMutableArray *labels;

@end
