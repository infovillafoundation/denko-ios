//
//  ViewController.h
//  DenkoStation
//
//  Created by Sandah Aung on 20/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>

#import "LanguageSelectionViewController.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController {
    NSLock* theLock;
    NSLock* languageSelectionLock;
    BOOL hasRunLoadingMain;
    BOOL hasLanguageSelectionRun;
}

@property (nonatomic) Reachability *internetReachability;

- (BOOL)isMyanmar;

@end

