//
//  Price.m
//  DenkoStation
//
//  Created by Sandah Aung on 16/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "Price.h"

@implementation Price

@synthesize id;
@synthesize ron95;
@synthesize ron92;
@synthesize dieselNormal;
@synthesize dieselSpecial;
@synthesize postDate;

@end
