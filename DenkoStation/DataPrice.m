//
//  DataPrice.m
//  DenkoStation
//
//  Created by Sandah Aung on 21/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "DataPrice.h"

@implementation DataPrice

@dynamic id;
@dynamic ron95;
@dynamic ron92;
@dynamic dieselNormal;
@dynamic dieselSpecial;
@dynamic postDate;

+ (instancetype)insertDataPriceWithPrice:(Price*)price
             inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    DataPrice* dataPrice = [NSEntityDescription insertNewObjectForEntityForName:self.entityName
                                               inManagedObjectContext:managedObjectContext];
    [dataPrice setId:price.id];
    dataPrice.ron95 = price.ron95;
    dataPrice.ron92 = price.ron92;
    dataPrice.dieselNormal = price.dieselNormal;
    dataPrice.dieselSpecial = price.dieselSpecial;
    dataPrice.postDate = price.postDate;
    
    NSError *error;
    [managedObjectContext save:&error];
    
    return dataPrice;
}

+ (NSString*)entityName
{
    return @"Price";
}

+ (instancetype)fetchDataPriceInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    NSEntityDescription *entity = [NSEntityDescription entityForName:self.entityName inManagedObjectContext:managedObjectContext];
    
    if (!entity) {
        return NULL;
    }
    
        
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"postDate" ascending:NO];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = entity;
    request.sortDescriptors = @[ sortDescriptor ];
    request.fetchLimit = 1;
    
    NSError *error;
    NSArray *fetchResults = [managedObjectContext executeFetchRequest:request error:&error];
    
    DataPrice *result = fetchResults.firstObject;
    
    return result;
}

+ (Price*)fetchPriceInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    DataPrice *dataPrice = [self fetchDataPriceInManagedObjectContext:managedObjectContext];
    
    Price *price = [[Price alloc] init];
    price.id = dataPrice.id;
    price.ron92 = dataPrice.ron92;
    price.ron95 = dataPrice.ron95;
    price.dieselNormal = dataPrice.dieselNormal;
    price.dieselSpecial = dataPrice.dieselSpecial;
    price.postDate = dataPrice.postDate;
    
    return price;
}


@end
