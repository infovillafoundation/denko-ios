//
//  Price.h
//  DenkoStation
//
//  Created by Sandah Aung on 16/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Price : NSObject {
    NSNumber *id;
    NSNumber *ron95;
    NSNumber *ron92;
    NSNumber *dieselNormal;
    NSNumber *dieselSpecial;
    NSDate *postDate;
}

@property (nonatomic, retain) NSNumber *id;
@property (nonatomic, retain) NSNumber *ron95;
@property (nonatomic, retain) NSNumber *ron92;
@property (nonatomic, retain) NSNumber *dieselNormal;
@property (nonatomic, retain) NSNumber *dieselSpecial;
@property (nonatomic, retain) NSDate *postDate;

@end
