//
//  LanguageSelectionViewController.h
//  DenkoStation
//
//  Created by Sandah Aung on 3/7/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDataModel.h"

@interface LanguageSelectionViewController : UIViewController {
    
    UIImageView *languageFlag;
    UILabel *languageText;
    UIImageView *countDownFlag;
    
    NSLock* theLock;
    BOOL hasRun;
    
    int width;
    int height;
    
    int i;
}

@property (strong, nonatomic) UIImageView *countDownFlag;

@end

