//
//  MainViewController.h
//  DenkoStation
//
//  Created by Sandah Aung on 23/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>


#import "LanguageSelectionViewController.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "Price.h"

@interface MainViewController : UIViewController {
    UILabel *fuelPriceText;
    UILabel *pDieselText;
    UILabel *qDieselText;
    UILabel *ron95Text;
    UILabel *ron92Text;
}

@property (nonatomic) Reachability *internetReachability;

- (void)displayInternetConnectivityMessage;

@end
