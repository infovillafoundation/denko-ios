//
//  StationViewController.m
//  DenkoStation
//
//  Created by Sandah Aung on 7/8/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "StationViewController.h"
#import "AppDelegate.h"

@interface StationViewController ()

@end

@implementation StationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if (height == 1024) {
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:720 y:65];
        [self placeNavIcon:@"NavIconOrange" x:15 y:55];
    } else if (height == 568) {
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:297 y:40];
        [self placeNavIcon:@"NavIconOrange" x:5 y:30];
    } else if (height == 480) {
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:297 y:40];
        [self placeNavIcon:@"NavIconOrange" x:5 y:30];
    }
    
    NSTimer* timer = [NSTimer timerWithTimeInterval:30.0f target:self selector:@selector(updateStationList) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)placeMyanmarFlag:(NSString *) imageName hilightedImage:(NSString *) hilightedImage x:(int)x y: (int)y {
    UIButton *myanmarFlagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *mmFlagNormal = [UIImage imageNamed:imageName];
    [myanmarFlagButton setImage:mmFlagNormal forState:UIControlStateNormal];
    UIImage *mmFlagHighlighted = [UIImage imageNamed:hilightedImage];
    [myanmarFlagButton setImage:mmFlagHighlighted forState:UIControlStateHighlighted];
    [myanmarFlagButton sizeToFit];
    myanmarFlagButton.frame = CGRectMake(x, y, myanmarFlagButton.frame.size.width, myanmarFlagButton.frame.size.height);
    [myanmarFlagButton addTarget:self action:@selector(loadLanguageSelectionView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myanmarFlagButton];
}

- (void)placeNavIcon:(NSString *) imageName x:(int)x y: (int)y {
    UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image.frame = CGRectMake(x, y, 60, 60);
    [self.view addSubview:image];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openDrawer)];
    singleTap.numberOfTapsRequired = 1;
    [image setUserInteractionEnabled:YES];
    [image addGestureRecognizer:singleTap];
}

- (void)reloadTable {
    
}

- (void)openDrawer{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
}

- (BOOL)isMyanmar {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"myanmar"];
    return state;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 40;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    return NULL;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)updateStationList {
    UIViewController *parentController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        UIViewController *languageSelectionController = [[LanguageSelectionViewController alloc] initWithNibName:nil bundle:nil];
        [languageSelectionController setModalPresentationStyle:UIModalPresentationCustom];
        [languageSelectionController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [parentController presentViewController:languageSelectionController animated:YES completion:nil];
    }];
}

- (void)loadLanguageSelectionView {
    UIViewController *parentController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        UIViewController *languageSelectionController = [[LanguageSelectionViewController alloc] initWithNibName:nil bundle:nil];
        [languageSelectionController setModalPresentationStyle:UIModalPresentationCustom];
        [languageSelectionController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [parentController presentViewController:languageSelectionController animated:YES completion:nil];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
