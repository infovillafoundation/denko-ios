//
//  ViewController.m
//  DenkoStation
//
//  Created by Sandah Aung on 20/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//


#import <CoreData/CoreData.h>

#import "ViewController.h"
#import "DataPrice.h"


@interface ViewController ()

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
        
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if (height == 1024) {
        [self loadBackgroundAndGradient:@"BackgroundIpad" x:484 y:735];
        [self makeDenkoTextIpad];
    } else if (height == 736) {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BackgroundIphone736"]];
        [self.view addSubview:backgroundImage];
    } else if (height == 667) {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BackgroundIphone667h"]];
        [self.view addSubview:backgroundImage];
    } else if (height == 568) {
        [self loadBackgroundAndGradient:@"BackgroundIphone568h" x:178 y:351];
        [self makeDenkoText:45];
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:25 y:45];
    } else if (height == 480) {
        [self loadBackgroundAndGradient:@"BackgroundIphone480h" x:178 y:306];
        [self makeDenkoText:0];
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:20 y:40];
    }
    
    theLock = [[NSLock alloc] init];
    hasRunLoadingMain = NO;
    languageSelectionLock = [[NSLock alloc] init];
    hasLanguageSelectionRun = NO;
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self displayInternetConnectivityMessage];
    
    [self performSelector:@selector(toMainView) withObject:nil afterDelay:2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)placeMyanmarFlag:(NSString *) imageName hilightedImage:(NSString *) hilightedImage x:(int)x y: (int)y {
    UIButton *myanmarFlagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *mmFlagNormal = [UIImage imageNamed:imageName];
    [myanmarFlagButton setImage:mmFlagNormal forState:UIControlStateNormal];
    UIImage *mmFlagHighlighted = [UIImage imageNamed:hilightedImage];
    [myanmarFlagButton setImage:mmFlagHighlighted forState:UIControlStateHighlighted];
    [myanmarFlagButton sizeToFit];
    myanmarFlagButton.frame = CGRectMake(x, y, myanmarFlagButton.frame.size.width, myanmarFlagButton.frame.size.height);
    [myanmarFlagButton addTarget:self action:@selector(toLanguageSelectionView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myanmarFlagButton];
}

- (void)loadBackgroundAndGradient:(NSString *) imageName x:(int)x y: (int)y {
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [self.view addSubview:backgroundImage];
    
    UIButton *gradientButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *orangeGradient = [UIImage imageNamed:@"GradientOrange"];
    [gradientButton setImage:orangeGradient forState:UIControlStateNormal];
    UIImage *greenGradient = [UIImage imageNamed:@"GradientGreen"];
    [gradientButton setImage:greenGradient forState:UIControlStateHighlighted];
    [gradientButton sizeToFit];
    gradientButton.frame = CGRectMake(x, y, gradientButton.frame.size.width, gradientButton.frame.size.height);
    [gradientButton addTarget:self action:@selector(toMainView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:gradientButton];
}

- (void)makeDenkoText:(int)yOffset {
    UILabel *welcomeText = [[UILabel alloc] initWithFrame:CGRectMake(190, 310 + yOffset, 150, 70)];
    welcomeText.text = @"BudKqdkyg\\";
    welcomeText.textColor = [UIColor whiteColor];
    [welcomeText setFont:[UIFont fontWithName:@"WinInnwa" size:20]];
    UILabel *welcomeTextGreatetThan = [[UILabel alloc] initWithFrame:CGRectMake(290, 310 + yOffset, 150, 70)];
    welcomeTextGreatetThan.text = @">";
    welcomeTextGreatetThan.textColor = [UIColor whiteColor];
    [welcomeTextGreatetThan setFont:[UIFont fontWithName:@"OpenSans" size:20]];
    UILabel *welcomeTextLongText = [[UILabel alloc] initWithFrame:CGRectMake(190, 340 + yOffset, 128, 75)];
    welcomeTextLongText.text = @"ypönf;rSef aps;EIef;rSef tcsiftwG,frSefjzifh t&nftaoG;tjrifhqHk; qDaumif;rsm;udk a&mif;csay;aeaom \'efudkqDqdkifrsm;";
    welcomeTextLongText.textColor = [UIColor whiteColor];
    [welcomeTextLongText setFont:[UIFont fontWithName:@"WinInnwa" size:10]];
    welcomeTextLongText.numberOfLines = 3;
    [self.view addSubview:welcomeText];
    [self.view addSubview:welcomeTextGreatetThan];
    [self.view addSubview:welcomeTextLongText];
}

- (void)makeDenkoTextIpad {
    UILabel *welcomeText = [[UILabel alloc] initWithFrame:CGRectMake(500, 790, 150, 70)];
    welcomeText.text = @"BudKqdkyg\\";
    welcomeText.textColor = [UIColor whiteColor];
    [welcomeText setFont:[UIFont fontWithName:@"WinInnwa" size:40]];
    UILabel *welcomeTextGreatetThan = [[UILabel alloc] initWithFrame:CGRectMake(650, 790, 150, 70)];
    welcomeTextGreatetThan.text = @">";
    welcomeTextGreatetThan.textColor = [UIColor whiteColor];
    [welcomeTextGreatetThan setFont:[UIFont fontWithName:@"OpenSans" size:40]];
    UILabel *welcomeTextLongText = [[UILabel alloc] initWithFrame:CGRectMake(500, 820, 256, 150)];
    welcomeTextLongText.text = @"ypönf;rSef aps;EIef;rSef tcsiftwG,frSefjzifh t&nftaoG;tjrifhqHk; qDaumif;rsm;udk a&mif;csay;aeaom \'efudkqDqdkifrsm;";
    welcomeTextLongText.textColor = [UIColor whiteColor];
    [welcomeTextLongText setFont:[UIFont fontWithName:@"WinInnwa" size:20]];
    welcomeTextLongText.numberOfLines = 3;
    [self.view addSubview:welcomeText];
    [self.view addSubview:welcomeTextGreatetThan];
    [self.view addSubview:welcomeTextLongText];
}

- (void)toMainView {
    [theLock lock];
    if (hasRunLoadingMain == YES || hasLanguageSelectionRun == YES) {
        [theLock unlock];
        return;
    }
    hasRunLoadingMain = YES;
    
    [self performSelector:@selector(loadMainView) withObject:nil afterDelay:1];
    
    [theLock unlock];
}

- (void)loadMainView {
    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)toLanguageSelectionView {
    if (hasLanguageSelectionRun == YES || hasRunLoadingMain == YES) {
        return;
    }
    hasLanguageSelectionRun = YES;
    
    [self loadLanguageSelectionView];
}

- (void)loadLanguageSelectionView {
    UIViewController *parentController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        UIViewController *languageSelectionController = [[LanguageSelectionViewController alloc] initWithNibName:nil bundle:nil];
        [languageSelectionController setModalPresentationStyle:UIModalPresentationCustom];
        [languageSelectionController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [parentController presentViewController:languageSelectionController animated:YES completion:nil];
    }];
}

- (BOOL)isMyanmar {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"myanmar"];
    return state;
}

- (void)displayInternetConnectivityMessage {
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    
    if (netStatus == NotReachable)      {
        if ([self isMyanmar])
            [self.view makeToast:@"tifwmeuf zGifhrxm;yg"];
        else
            [self.view makeToast:@"No internet connection"];
    } else {
        NSURL *baseURL = [NSURL URLWithString:@"http://128.199.186.47:8080/DenkoStationServer-1/denkoservice/price"];
        NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
        
        RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Price class]];
        [mapping addAttributeMappingsFromDictionary:@{@"id": @"id", @"ron95": @"ron95", @"ron92": @"ron92", @"dieselNormal": @"dieselNormal", @"dieselSpecial": @"dieselSpecial"}];
        
        RKBlockValueTransformer *numberToDateValueTransformer = [RKBlockValueTransformer valueTransformerWithValidationBlock:^BOOL(__unsafe_unretained Class sourceClass, __unsafe_unretained Class destinationClass) {
            // This transformer handles `NSNumber` <-> `NSDate` transformations
            return (([sourceClass isSubclassOfClass:[NSNumber class]] && [destinationClass isSubclassOfClass:[NSDate class]]) ||
                    ([sourceClass isSubclassOfClass:[NSDate class]] && [destinationClass isSubclassOfClass:[NSNumber class]]));
        } transformationBlock:^BOOL(id inputValue, __autoreleasing id *outputValue, __unsafe_unretained Class outputValueClass, NSError *__autoreleasing *error) {
            RKValueTransformerTestInputValueIsKindOfClass(inputValue, (@[ [NSNumber class], [NSDate class] ]), error);
            RKValueTransformerTestOutputValueClassIsSubclassOfClass(outputValueClass, (@[ [NSNumber class], [NSDate class] ]), error);
            if ([outputValueClass isSubclassOfClass:[NSDate class]]) {
                if ([inputValue isKindOfClass:[NSNumber class]]) {
                    *outputValue = [NSDate dateWithTimeIntervalSince1970:[inputValue doubleValue] / 1000];
                }
            } else if ([outputValueClass isSubclassOfClass:[NSNumber class]]) {
                *outputValue = @([inputValue timeIntervalSince1970]);
            }
            return YES;
        }];
        
        
        RKAttributeMapping *postDateMapping = [RKAttributeMapping attributeMappingFromKeyPath:@"postDate" toKeyPath:@"postDate"];
        postDateMapping.valueTransformer = numberToDateValueTransformer;
        [mapping addPropertyMapping:postDateMapping];
        
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodGET pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        
        RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[ responseDescriptor ]];
        [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            if ([self isMyanmar])
                [self.view makeToast:@"qmAmESifh csdwfqufjcif; atmifjrifonf"];
            else
                [self.view makeToast:@"Server connection successful"];
            NSArray *prices = [mappingResult array];
            NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                                sortDescriptorWithKey:@"postDate"
                                                ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
            NSArray *pricesSorted = [prices
                                     sortedArrayUsingDescriptors:sortDescriptors];
            for (id price in pricesSorted) {
                NSLog(@"ron 92 = %@", [price ron92]);
                NSLog(@"ron 95 = %@", [price ron95]);
                NSLog(@"diesel = %@", [price dieselNormal]);
                NSLog(@"diesel special = %@", [price dieselSpecial]);
                NSLog(@"post date = %@", [price postDate]);
            }
            [self updateLocalPrice:pricesSorted.firstObject];
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NSLog(@"error: %@", [error localizedFailureReason]);
            if ([self isMyanmar])
                [self.view makeToast:@"qmAmESifh qufoG,fr&yg"];
            else
                [self.view makeToast:@"Connection with server failed"];
        }];
        
        [objectRequestOperation start];
    }
}

- (void)updateLocalPrice: (Price*) price {
    NSManagedObjectContext *managedObjectContext = [AppDelegate globalDelegate].persistentStack.managedObjectContext;
    DataPrice *returnedDataPrice = [DataPrice fetchDataPriceInManagedObjectContext:managedObjectContext];
    
    if (returnedDataPrice) {
        returnedDataPrice.ron92 = price.ron92;
        returnedDataPrice.ron95 = price.ron95;
        returnedDataPrice.dieselNormal = price.dieselNormal;
        returnedDataPrice.dieselSpecial = price.dieselSpecial;
        returnedDataPrice.postDate = price.postDate;
        
        NSError *error = nil;
        [managedObjectContext save:&error];
    } else {
        price.id = [NSNumber numberWithInt:1];
        [DataPrice insertDataPriceWithPrice:price inManagedObjectContext:managedObjectContext];
    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:true forKey:@"priceSet"];
    [userdefaults synchronize];
}


@end
