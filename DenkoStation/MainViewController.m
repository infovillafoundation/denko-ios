//
//  MainViewController.m
//  DenkoStation
//
//  Created by Sandah Aung on 23/6/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "DataPrice.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if (height == 1024) {
        [self loadBackground:@"MainViewIpad"];
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:720 y:65];
        [self placeNavIcon:@"NavIconOrange" x:15 y:55];
        [self placeGreenBoxes:@"BoxWithGasPumpIpad" x1:18 y1:810 x2:390 y2:810 x3:18 y3:910 x4:390 y4:910];
        
        [self writeIpadFuelPriceTextX:61 y:734 width:149 height:47];
    } else if (height == 568) {
        int offset = 85;
        [self loadBackground:@"MainView@568h"];
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:297 y:40];
        [self placeNavIcon:@"NavIconOrange" x:5 y:30];
        [self placeGreenBoxes:@"BoxWithGasPump" x1:9 y1:375+offset x2:162 y2:375+offset x3:9 y3:425+offset x4:162 y4:425+offset];
        
        [self writeFuelPriceTextX:9 y:339+offset width:76 height:25];
    } else if (height == 480) {
        [self loadBackground:@"MainView@480h"];
        [self placeMyanmarFlag:@"MyanmarFlag" hilightedImage:@"MyanmarFlagHilighted" x:297 y:40];
        [self placeNavIcon:@"NavIconOrange" x:5 y:30];
        [self placeGreenBoxes:@"BoxWithGasPump" x1:9 y1:375 x2:162 y2:375 x3:9 y3:425 x4:162 y4:425];
        
        [self writeFuelPriceTextX:9 y:339 width:76 height:25];
    }
    
    [self setPrice];
    
    NSTimer* timer = [NSTimer timerWithTimeInterval:30.0f target:self selector:@selector(updateDualLanguageTextAndPrices) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

-(void)viewDidAppear:(BOOL)animated {
    if ([AppDelegate globalDelegate].splashScreenLoaded == NO) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *uiViewController = [storyboard instantiateViewControllerWithIdentifier:@"splashViewController"];
        [uiViewController setModalPresentationStyle:UIModalPresentationCustom];
        [uiViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:uiViewController animated:YES completion:nil];
        [AppDelegate globalDelegate].splashScreenLoaded = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPrice {
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if ([self priceHasBeenSet]) {
    
        Price *price = [self getCurrentPrice];
    
        if (height == 1024) {
            [self writeIpadEachPriceText:pDieselText text:@"Wait for price" x:18 y:810 width:360 height:90];
            [self writeIpadEachPriceText:qDieselText text:@"Wait for price" x:390 y:810 width:360 height:90];
            [self writeIpadEachPriceText:ron95Text text:@"Wait for price" x:18 y:910 width:360 height:90];
            [self writeIpadEachPriceText:ron92Text text:@"Wait for price" x:390 y:910 width:360 height:90];
        } else if (height == 568) {
            int offset = 85;
            [self writeEachPriceText:pDieselText text:[NSString stringWithFormat:@"P Diesel: %.02f Ks", price.dieselSpecial.floatValue] x:9 y:375+offset width:149 height:46];
            [self writeEachPriceText:qDieselText text:[NSString stringWithFormat:@"Diesel: %.02f Ks", price.dieselNormal.floatValue] x:162 y:375+offset width:149 height:46];
            [self writeEachPriceText:ron95Text text:[NSString stringWithFormat:@"Ron 95: %.02f Ks", price.ron95.floatValue] x:9 y:425+offset width:149 height:46];
            [self writeEachPriceText:ron92Text text:[NSString stringWithFormat:@"Ron 92: %.02f Ks", price.ron92.floatValue] x:162 y:425+offset width:149 height:46];
        } else if (height == 480) {
            [self writeEachPriceText:pDieselText text:[NSString stringWithFormat:@"P Diesel: %.02f", price.dieselSpecial.floatValue] x:9 y:375 width:149 height:46];
            [self writeEachPriceText:qDieselText text:[NSString stringWithFormat:@"Diesel: %.02f Ks", price.dieselNormal.floatValue] x:162 y:375 width:149 height:46];
            [self writeEachPriceText:ron95Text text:[NSString stringWithFormat:@"Ron 95: %.02f Ks", price.ron95.floatValue] x:9 y:425 width:149 height:46];
            [self writeEachPriceText:ron92Text text:[NSString stringWithFormat:@"Ron 92: %.02f Ks", price.ron92.floatValue] x:162 y:425 width:149 height:46];
        }
    } else {
        if (height == 1024) {
            [self writeIpadEachPriceText:pDieselText text:@"Wait for price" x:18 y:810 width:360 height:90];
            [self writeIpadEachPriceText:qDieselText text:@"Wait for price" x:390 y:810 width:360 height:90];
            [self writeIpadEachPriceText:ron95Text text:@"Wait for price" x:18 y:910 width:360 height:90];
            [self writeIpadEachPriceText:ron92Text text:@"Wait for price" x:390 y:910 width:360 height:90];
        } else if (height == 568) {
            int offset = 85;
            [self writeEachPriceText:pDieselText text:@"Wait for price" x:9 y:375+offset width:149 height:46];
            [self writeEachPriceText:qDieselText text:@"Wait for price" x:162 y:375+offset width:149 height:46];
            [self writeEachPriceText:ron95Text text:@"Wait for price" x:9 y:425+offset width:149 height:46];
            [self writeEachPriceText:ron92Text text:@"Wait for price" x:162 y:425+offset width:149 height:46];
        } else if (height == 480) {
            [self writeEachPriceText:pDieselText text:@"Wait for price" x:9 y:375 width:149 height:46];
            [self writeEachPriceText:qDieselText text:@"Wait for price" x:162 y:375 width:149 height:46];
             [self writeEachPriceText:ron95Text text:@"Wait for price" x:9 y:425 width:149 height:46];
              [self writeEachPriceText:ron92Text text:@"Wait for price" x:162 y:425 width:149 height:46];
        }
    }
}

- (void)updatePrice {
    if ([self priceHasBeenSet]) {
        Price *price = [self getCurrentPrice];
        [pDieselText setText:[NSString stringWithFormat:@"P Diesel: %.02f Ks", price.dieselSpecial.floatValue]];
        [qDieselText setText:[NSString stringWithFormat:@"Diesel: %.02f Ks", price.dieselNormal.floatValue]];
        [ron95Text setText:[NSString stringWithFormat:@"Ron 95: %.02f Ks", price.ron95.floatValue]];
        [ron92Text setText:[NSString stringWithFormat:@"Ron 92: %.02f Ks", price.ron92.floatValue]];
    }
}

- (void)loadBackground:(NSString *) imageName {
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [self.view addSubview:backgroundImage];
}

- (void)updateDualLanguageTextAndPrices {
    [self updateFuelPriceText];
    [self updatePrice];
}

- (void)placeMyanmarFlag:(NSString *) imageName hilightedImage:(NSString *) hilightedImage x:(int)x y: (int)y {
    UIButton *myanmarFlagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *mmFlagNormal = [UIImage imageNamed:imageName];
    [myanmarFlagButton setImage:mmFlagNormal forState:UIControlStateNormal];
    UIImage *mmFlagHighlighted = [UIImage imageNamed:hilightedImage];
    [myanmarFlagButton setImage:mmFlagHighlighted forState:UIControlStateHighlighted];
    [myanmarFlagButton sizeToFit];
    myanmarFlagButton.frame = CGRectMake(x, y, myanmarFlagButton.frame.size.width, myanmarFlagButton.frame.size.height);
    [myanmarFlagButton addTarget:self action:@selector(loadLanguageSelectionView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myanmarFlagButton];
}

- (void)placeNavIcon:(NSString *) imageName x:(int)x y: (int)y {
    UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image.frame = CGRectMake(x, y, 60, 60);
    [self.view addSubview:image];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openDrawer)];
    singleTap.numberOfTapsRequired = 1;
    [image setUserInteractionEnabled:YES];
    [image addGestureRecognizer:singleTap];
}

- (void)placeGreenBoxes:(NSString *) imageName x1:(int)x1 y1: (int)y1 x2:(int)x2 y2: (int)y2 x3:(int)x3 y3: (int)y3 x4:(int)x4 y4: (int)y4 {
    UIImageView *image1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image1.frame = CGRectMake(x1, y1, image1.frame.size.width, image1.frame.size.height);
    [self.view addSubview:image1];
    
    UIImageView *image2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image2.frame = CGRectMake(x2, y2, image2.frame.size.width, image2.frame.size.height);
    [self.view addSubview:image2];
    
    UIImageView *image3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image3.frame = CGRectMake(x3, y3, image3.frame.size.width, image3.frame.size.height);
    [self.view addSubview:image3];
    
    UIImageView *image4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    image4.frame = CGRectMake(x4, y4, image4.frame.size.width, image4.frame.size.height);
    [self.view addSubview:image4];
}

- (void)writeFuelPriceTextX: (int) x y: (int) y width: (int) width height: (int) height {
    fuelPriceText = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    fuelPriceText.textColor = [UIColor whiteColor];
    fuelPriceText.textAlignment = NSTextAlignmentCenter;
    if ([self isMyanmar]) {
        [fuelPriceText setFont:[UIFont fontWithName:@"WinInnwa" size:13]];
        fuelPriceText.text = @"qDaps;EIef;rsm;";
    } else {
        [fuelPriceText setFont:[UIFont fontWithName:@"OpenSans" size:11]];
        fuelPriceText.text = @"Fuel Prices";
    }
    [self.view addSubview:fuelPriceText];
}

- (void)writeIpadFuelPriceTextX: (int) x y: (int) y width: (int) width height: (int) height {
    fuelPriceText = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    fuelPriceText.textColor = [UIColor whiteColor];
    fuelPriceText.textAlignment = NSTextAlignmentCenter;
    if ([self isMyanmar]) {
        [fuelPriceText setFont:[UIFont fontWithName:@"WinInnwa" size:25]];
        fuelPriceText.text = @"qDaps;EIef;rsm;";
    } else {
        [fuelPriceText setFont:[UIFont fontWithName:@"OpenSans" size:20]];
        fuelPriceText.text = @"Fuel Prices";
    }
    [self.view addSubview:fuelPriceText];
}

- (void)updateFuelPriceText {
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if (height == 1024) {
        if ([self isMyanmar]) {
            [fuelPriceText setFont:[UIFont fontWithName:@"WinInnwa" size:25]];
            fuelPriceText.text = @"qDaps;EIef;rsm;";
        } else {
            [fuelPriceText setFont:[UIFont fontWithName:@"OpenSans" size:20]];
            fuelPriceText.text = @"Fuel Prices";
        }
    } else {
        if ([self isMyanmar]) {
            [fuelPriceText setFont:[UIFont fontWithName:@"WinInnwa" size:13]];
            fuelPriceText.text = @"qDaps;EIef;rsm;";
        } else {
            [fuelPriceText setFont:[UIFont fontWithName:@"OpenSans" size:11]];
            fuelPriceText.text = @"Fuel Prices";
        }
    }
    
}

- (void)writeEachPriceText:(UILabel*) label text: (NSString *) textToWrite x: (int) x y: (int) y width: (int) width height: (int) height {
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    text.text = textToWrite;
    text.textColor = [UIColor whiteColor];
    text.textAlignment = NSTextAlignmentCenter;
    [text setFont:[UIFont fontWithName:@"OpenSans" size:12]];
    [self.view addSubview:text];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fetchPrice)];
    singleTap.numberOfTapsRequired = 1;
    [text setUserInteractionEnabled:YES];
    [text addGestureRecognizer:singleTap];
}

- (void)writeIpadEachPriceText:(UILabel*) label text: (NSString *) textToWrite x: (int) x y: (int) y width: (int) width height: (int) height {
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    text.text = textToWrite;
    text.textColor = [UIColor whiteColor];
    text.textAlignment = NSTextAlignmentCenter;
    [text setFont:[UIFont fontWithName:@"OpenSans" size:24]];
    [self.view addSubview:text];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fetchPrice)];
    singleTap.numberOfTapsRequired = 1;
    [text setUserInteractionEnabled:YES];
    [text addGestureRecognizer:singleTap];
}

- (BOOL)isMyanmar {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"myanmar"];
    return state;
}

- (BOOL)priceHasBeenSet {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    BOOL state = [userdefaults boolForKey:@"priceSet"];
    return state;
}

- (void)loadLanguageSelectionView {
    UIViewController *languageSelectionController = [[LanguageSelectionViewController alloc] initWithNibName:nil bundle:nil];
    [languageSelectionController setModalPresentationStyle:UIModalPresentationCustom];
    [languageSelectionController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:languageSelectionController animated:YES completion:nil];
}

-(void)openDrawer{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
}

-(Price*)getCurrentPrice{
    NSManagedObjectContext *managedObjectContext = [AppDelegate globalDelegate].persistentStack.managedObjectContext;
    return [DataPrice fetchPriceInManagedObjectContext:managedObjectContext];
}

-(void)fetchPrice{
    if ([self isMyanmar])
        [self.view makeToast:@"aps;EIef; ppfaq;aeonf"];
    else
        [self.view makeToast:@"Checking price"];
    
    
    [self performSelector:@selector(displayInternetConnectivityMessage) withObject:nil afterDelay:1];
}

- (void)displayInternetConnectivityMessage {
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    
    if (netStatus == NotReachable)      {
        if ([self isMyanmar])
            [self.view makeToast:@"tifwmeuf zGifhrxm;yg"];
        else
            [self.view makeToast:@"No internet connection"];
    } else {
        NSURL *baseURL = [NSURL URLWithString:@"http://128.199.186.47:8080/DenkoStationServer-1/denkoservice/price"];
        NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
        
        RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Price class]];
        [mapping addAttributeMappingsFromDictionary:@{@"id": @"id", @"ron95": @"ron95", @"ron92": @"ron92", @"dieselNormal": @"dieselNormal", @"dieselSpecial": @"dieselSpecial"}];
        
        RKBlockValueTransformer *numberToDateValueTransformer = [RKBlockValueTransformer valueTransformerWithValidationBlock:^BOOL(__unsafe_unretained Class sourceClass, __unsafe_unretained Class destinationClass) {
            // This transformer handles `NSNumber` <-> `NSDate` transformations
            return (([sourceClass isSubclassOfClass:[NSNumber class]] && [destinationClass isSubclassOfClass:[NSDate class]]) ||
                    ([sourceClass isSubclassOfClass:[NSDate class]] && [destinationClass isSubclassOfClass:[NSNumber class]]));
        } transformationBlock:^BOOL(id inputValue, __autoreleasing id *outputValue, __unsafe_unretained Class outputValueClass, NSError *__autoreleasing *error) {
            RKValueTransformerTestInputValueIsKindOfClass(inputValue, (@[ [NSNumber class], [NSDate class] ]), error);
            RKValueTransformerTestOutputValueClassIsSubclassOfClass(outputValueClass, (@[ [NSNumber class], [NSDate class] ]), error);
            if ([outputValueClass isSubclassOfClass:[NSDate class]]) {
                if ([inputValue isKindOfClass:[NSNumber class]]) {
                    *outputValue = [NSDate dateWithTimeIntervalSince1970:[inputValue doubleValue] / 1000];
                }
            } else if ([outputValueClass isSubclassOfClass:[NSNumber class]]) {
                *outputValue = @([inputValue timeIntervalSince1970]);
            }
            return YES;
        }];
        
        
        RKAttributeMapping *postDateMapping = [RKAttributeMapping attributeMappingFromKeyPath:@"postDate" toKeyPath:@"postDate"];
        postDateMapping.valueTransformer = numberToDateValueTransformer;
        [mapping addPropertyMapping:postDateMapping];
        
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodGET pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        
        RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[ responseDescriptor ]];
        [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            if ([self isMyanmar])
                [self.view makeToast:@"qmAmESifh csdwfqufjcif; atmifjrifonf"];
            else
                [self.view makeToast:@"Server connection successful"];
            NSArray *prices = [mappingResult array];
            NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                                sortDescriptorWithKey:@"postDate"
                                                ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
            NSArray *pricesSorted = [prices
                                     sortedArrayUsingDescriptors:sortDescriptors];
            for (id price in pricesSorted) {
                NSLog(@"ron 92 = %@", [price ron92]);
                NSLog(@"ron 95 = %@", [price ron95]);
                NSLog(@"diesel = %@", [price dieselNormal]);
                NSLog(@"diesel special = %@", [price dieselSpecial]);
                NSLog(@"post date = %@", [price postDate]);
            }
            [self updateLocalPrice:pricesSorted.firstObject];
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NSLog(@"error: %@", [error localizedFailureReason]);
            if ([self isMyanmar])
                [self.view makeToast:@"qmAmESifh qufoG,fr&yg"];
            else
                [self.view makeToast:@"Connection with server failed"];
        }];
        
        [objectRequestOperation start];
    }
}

- (void)updateLocalPrice: (Price*) price {
    NSManagedObjectContext *managedObjectContext = [AppDelegate globalDelegate].persistentStack.managedObjectContext;
    DataPrice *returnedDataPrice = [DataPrice fetchDataPriceInManagedObjectContext:managedObjectContext];
    
    if (returnedDataPrice) {
        returnedDataPrice.ron92 = price.ron92;
        returnedDataPrice.ron95 = price.ron95;
        returnedDataPrice.dieselNormal = price.dieselNormal;
        returnedDataPrice.dieselSpecial = price.dieselSpecial;
        returnedDataPrice.postDate = price.postDate;
        
        NSError *error = nil;
        [managedObjectContext save:&error];
    } else {
        price.id = [NSNumber numberWithInt:1];
        [DataPrice insertDataPriceWithPrice:price inManagedObjectContext:managedObjectContext];
    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setBool:true forKey:@"priceSet"];
    [userdefaults synchronize];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
