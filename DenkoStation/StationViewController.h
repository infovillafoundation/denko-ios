//
//  StationViewController.h
//  DenkoStation
//
//  Created by Sandah Aung on 7/8/15.
//  Copyright (c) 2015 InfovillaFoundation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
}

@property (nonatomic, strong) UITableView *stationTableView;

@end
